json.array!(@question_attachments) do |question_attachment|
  json.extract! question_attachment, :id, :question_id, :file_upload_id, :display_order
  json.url question_attachment_url(question_attachment, format: :json)
end
