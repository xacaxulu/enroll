json.array!(@questions) do |question|
  json.extract! question, :id, :display_text, :question_type, :help_text
  json.url question_url(question, format: :json)
end
