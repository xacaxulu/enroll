json.array!(@users) do |user|
  json.extract! user, :id, :first_name, :middle_initial, :last_name, :street_address, :street_address1, :city, :state, :zipcode, :country, :phone
  json.url user_url(user, format: :json)
end
