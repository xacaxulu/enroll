json.extract! @user, :id, :first_name, :middle_initial, :last_name, :street_address, :street_address1, :city, :state, :zipcode, :country, :phone, :created_at, :updated_at
