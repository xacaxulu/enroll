json.array!(@submission_questions) do |submission_question|
  json.extract! submission_question, :id, :submission_id, :question_id, :position
  json.url submission_question_url(submission_question, format: :json)
end
