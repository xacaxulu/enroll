class SubmissionQuestionsController < ApplicationController
  before_action :set_submission_question, only: [:show, :edit, :update, :destroy]

  # GET /submission_questions
  # GET /submission_questions.json
  def index
    @submission_questions = SubmissionQuestion.all
  end

  # GET /submission_questions/1
  # GET /submission_questions/1.json
  def show
  end

  # GET /submission_questions/new
  def new
    @submission_question = SubmissionQuestion.new
  end

  # GET /submission_questions/1/edit
  def edit
  end

  # POST /submission_questions
  # POST /submission_questions.json
  def create
    @submission_question = SubmissionQuestion.new(submission_question_params)

    respond_to do |format|
      if @submission_question.save
        format.html { redirect_to @submission_question, notice: 'Submission question was successfully created.' }
        format.json { render :show, status: :created, location: @submission_question }
      else
        format.html { render :new }
        format.json { render json: @submission_question.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /submission_questions/1
  # PATCH/PUT /submission_questions/1.json
  def update
    respond_to do |format|
      if @submission_question.update(submission_question_params)
        format.html { redirect_to @submission_question, notice: 'Submission question was successfully updated.' }
        format.json { render :show, status: :ok, location: @submission_question }
      else
        format.html { render :edit }
        format.json { render json: @submission_question.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /submission_questions/1
  # DELETE /submission_questions/1.json
  def destroy
    @submission_question.destroy
    respond_to do |format|
      format.html { redirect_to submission_questions_url, notice: 'Submission question was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_submission_question
      @submission_question = SubmissionQuestion.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def submission_question_params
      params.require(:submission_question).permit(:submission_id, :question_id, :position)
    end
end
