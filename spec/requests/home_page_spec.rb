require 'rails_helper'

RSpec.describe "Home Page", :type => :request do
  describe "GET /" do
    it "fetches homepage" do
      get root_path
      expect(response.status).to be(200)
    end
  end
end

RSpec.describe "Home Page", :type => :feature do
  before do
    @user = FactoryGirl.create(:user)
  end

  describe "GET /" do
    it 'fetches /' do
      visit '/'
      expect(page).to have_content('Welcome')
    end

    it 'has a login button for applicants' do
      visit '/'
      click_link('sign in')
      fill_in('user_email', :with => @user.email)
      fill_in('user_password', :with => @user.password)
      click_button('Sign in')
      expect(current_path).to eq '/'
    end
  end
end

