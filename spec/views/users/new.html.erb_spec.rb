require 'rails_helper'

RSpec.describe "users/new", :type => :view do
  before(:each) do
    assign(:user, User.new(
      :first_name => "MyString",
      :middle_initial => "MyString",
      :last_name => "MyString",
      :street_address => "MyString",
      :street_address1 => "MyString",
      :city => "MyString",
      :state => "MyString",
      :zipcode => "MyString",
      :country => "MyString",
      :phone => "MyString"
    ))
  end

  it "renders new user form" do
    render

    assert_select "form[action=?][method=?]", users_path, "post" do

      assert_select "input#user_first_name[name=?]", "user[first_name]"

      assert_select "input#user_middle_initial[name=?]", "user[middle_initial]"

      assert_select "input#user_last_name[name=?]", "user[last_name]"

      assert_select "input#user_street_address[name=?]", "user[street_address]"

      assert_select "input#user_street_address1[name=?]", "user[street_address1]"

      assert_select "input#user_city[name=?]", "user[city]"

      assert_select "input#user_state[name=?]", "user[state]"

      assert_select "input#user_zipcode[name=?]", "user[zipcode]"

      assert_select "input#user_country[name=?]", "user[country]"

      assert_select "input#user_phone[name=?]", "user[phone]"
    end
  end
end
