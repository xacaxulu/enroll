require 'rails_helper'

RSpec.describe "users/index", :type => :view do
  before(:each) do
    assign(:users, [
      User.create!(
        :first_name => "First Name",
        :middle_initial => "Middle Initial",
        :last_name => "Last Name",
        :street_address => "Street Address",
        :street_address1 => "Street Address1",
        :city => "City",
        :state => "State",
        :zipcode => "Zipcode",
        :country => "Country",
        :phone => "Phone"
      ),
      User.create!(
        :first_name => "First Name",
        :middle_initial => "Middle Initial",
        :last_name => "Last Name",
        :street_address => "Street Address",
        :street_address1 => "Street Address1",
        :city => "City",
        :state => "State",
        :zipcode => "Zipcode",
        :country => "Country",
        :phone => "Phone"
      )
    ])
  end

  it "renders a list of users" do
    render
    assert_select "tr>td", :text => "First Name".to_s, :count => 2
    assert_select "tr>td", :text => "Middle Initial".to_s, :count => 2
    assert_select "tr>td", :text => "Last Name".to_s, :count => 2
    assert_select "tr>td", :text => "Street Address".to_s, :count => 2
    assert_select "tr>td", :text => "Street Address1".to_s, :count => 2
    assert_select "tr>td", :text => "City".to_s, :count => 2
    assert_select "tr>td", :text => "State".to_s, :count => 2
    assert_select "tr>td", :text => "Zipcode".to_s, :count => 2
    assert_select "tr>td", :text => "Country".to_s, :count => 2
    assert_select "tr>td", :text => "Phone".to_s, :count => 2
  end
end
