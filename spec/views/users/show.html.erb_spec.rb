require 'rails_helper'

RSpec.describe "users/show", :type => :view do
  before(:each) do
    @user = assign(:user, User.create!(
      :first_name => "First Name",
      :middle_initial => "Middle Initial",
      :last_name => "Last Name",
      :street_address => "Street Address",
      :street_address1 => "Street Address1",
      :city => "City",
      :state => "State",
      :zipcode => "Zipcode",
      :country => "Country",
      :phone => "Phone"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/First Name/)
    expect(rendered).to match(/Middle Initial/)
    expect(rendered).to match(/Last Name/)
    expect(rendered).to match(/Street Address/)
    expect(rendered).to match(/Street Address1/)
    expect(rendered).to match(/City/)
    expect(rendered).to match(/State/)
    expect(rendered).to match(/Zipcode/)
    expect(rendered).to match(/Country/)
    expect(rendered).to match(/Phone/)
  end
end
