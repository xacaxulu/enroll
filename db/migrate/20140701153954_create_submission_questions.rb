class CreateSubmissionQuestions < ActiveRecord::Migration
  def change
    create_table :submission_questions do |t|
      t.integer :submission_id
      t.integer :question_id
      t.integer :position

      t.timestamps
    end
  end
end
