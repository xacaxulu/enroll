class CreateQuestionAttachments < ActiveRecord::Migration
  def change
    create_table :question_attachments do |t|
      t.integer :question_id
      t.integer :file_upload_id
      t.integer :display_order

      t.timestamps
    end
  end
end
