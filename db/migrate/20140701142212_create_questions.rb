class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :display_text
      t.string :question_type
      t.string :help_text

      t.timestamps
    end
  end
end
