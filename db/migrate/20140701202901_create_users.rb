class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :middle_initial
      t.string :last_name
      t.string :street_address
      t.string :street_address1
      t.string :city
      t.string :state
      t.string :zipcode
      t.string :country
      t.string :phone

      t.timestamps
    end
  end
end
