class CreateSubmissionApplicants < ActiveRecord::Migration
  def change
    create_table :submission_applicants do |t|
      t.integer :applicant_id
      t.integer :submission_id

      t.timestamps
    end
  end
end
